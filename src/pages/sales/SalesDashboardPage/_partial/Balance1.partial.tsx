import React, { useState, useEffect } from 'react';
import Card, { CardBody } from '../../../../components/ui/Card';
import Icon from '../../../../components/icon/Icon';
import Tooltip from '../../../../components/ui/Tooltip';
import Balance from '../../../../components/Balance';
import { TPeriod } from '../../../../constants/periods.constant';
import SocketClient from '../../../../services/socket-client';
import { accelerometerClient } from '../../../../services/accelerometer';

const Balance1Partial = ({ activeTab }: { activeTab: TPeriod }) => {
	const [totalRecords, setTotalRecords] = useState(0);

	
	useEffect(() => {
		const socket = SocketClient.getInstance();
		
		// consultamos la api para obtener el total de registros
		const fetchData = async () => {
			try {
				const response = await accelerometerClient.getAccelerometer();
				setTotalRecords(response.count); // Actualiza el estado con los datos de la API
			} catch (error) {
				console.error('Error fetching accelerometer data:', error);
			}
		};

		fetchData();
		
		// Evento que se emite cuando hay nuevos datos del acelerómetro
		socket.onAccelerometerData((data) => {
			// Actualizar la cantidad total de registros y otros datos según tus necesidades
			setTotalRecords(data.id);

			// Puedes acceder al ID del nuevo registro desde 'data.id'
			const nuevoRegistroId = data.id;
			// Haz lo que necesites con el nuevo ID, por ejemplo, mostrarlo en la consola
			//console.log('Nuevo ID de registro:', nuevoRegistroId);
		});

		// Limpiar la suscripción cuando el componente se desmonta
		return () => {
			socket.offAccelerometerData();
		};
	}, []); // Dependencia vacía para que se ejecute solo al montar y desmontar el componente

	return (
		<Card>
			<CardBody>
				<div className='flex flex-col gap-2'>
					<div className='flex h-16 w-16 items-center justify-center rounded-full bg-blue-500'>
						<Icon icon='HeroCalendar' size='text-3xl' className='text-white' />
					</div>
					<div className='space-x-1 text-zinc-500 rtl:space-x-reverse'>
						<span className='font-semibold'>Todos los Registros</span>
						<Tooltip text='Todos los registros creados en tiempo real' />
					</div>
					<div className='text-4xl font-semibold'>{totalRecords}</div>
					{/* <div className='flex'>
						<Balance status='positive' value='32%'>
							Balance
						</Balance>
					</div> */}
				</div>
			</CardBody>
		</Card>
	);
};

export default Balance1Partial;
