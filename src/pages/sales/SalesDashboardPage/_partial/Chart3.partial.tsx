import { useState, useEffect } from 'react';
import Card, {
	CardBody,
	CardHeader,
	CardHeaderChild,
	CardTitle,
} from '../../../../components/ui/Card';
import Chart from '../../../../components/Chart';
import { IChartOptions } from '../../../../interface/chart.interface';
import { accelerometerClient } from '../../../../services/accelerometer';

const ChartRMSY = () => {
	const [state, setChartData] = useState<IChartOptions>({
		series: [
			{
				name: 'Max',
				data: [],
			},
			{
				name: 'Min',
				data: [],
			},
			{
				name: 'Avg',
				data: [],
			},
		],
		options: {
			chart: {
				id: 'realtime',
				height: 400,
				type: 'candlestick',
				stacked: false,
				animations: {
					enabled: true,
					easing: 'linear',
					dynamicAnimation: {
						speed: 1000,
					},
				},
				toolbar: {
					show: true,
				},
				zoom: {
					enabled: false,
				},
			},
			dataLabels: {
				enabled: false,
			},
			plotOptions: {
				candlestick: {
					colors: {
						upward: '#00ff08', // Color de las velas alcistas (cierre > apertura)
						downward: '#FF0000', // Color de las velas bajistas (cierre <= apertura)
					},
				},
			},
			xaxis: {
				categories: [], // Inicialmente vacío
				type: 'numeric', // Cambiado a 'numeric' para representar números en el eje X
			},
			yaxis: {
				title: {
					text: 'RMS',
				},
				tooltip: {
					enabled: true,
				},
			},
			fill: {
				opacity: 1,
			},
			tooltip: {
				shared: true,
				intersect: false,
				y: {
					formatter(y) {
						return y.toFixed(2);
					},
				},
			},
		},
	});

	/* creamos un useEffect para que se ejecute cuando se monte el componente
	y asi mismo poder mostrar los datos en tiempo real de nuestro Socket */
	useEffect(() => {
		// Llamada a la API getAccelerometer para obtener los datos
		const fetchData = async () => {
			try {
				//const response = await accelerometerClient.getAccelerometer();
				const response = await accelerometerClient.getAccelerometerStats('y');
				if (response.data && response.data.length > 0) {
					const newData = response.data.map((entry, index) => ({
						x: index + 1,
						y: [
							entry.avg + 100, // Apertura (en este caso, se uso la media + 100 para que se vea mejor en el gráfico)
							entry.max, // Máximo
							entry.min, // Mínimo
							entry.avg, // Cierre (en este caso, se uso la media)
						],
					}));
					setChartData({
						...state,
						series: [
							{
								name: 'RMS',
								data: newData,
							},
						],
						options: {
							...state.options,
							xaxis: {
								...state.options.xaxis,
								categories: newData.map((entry) => entry.x),
							},
						},
					});
				} else {
					console.warn('No data received from the API');
				}
			} catch (error) {
				console.error('Error fetching accelerometer data:', error);
			}
		};

		// Llamada inicial para obtener los datos
		fetchData();

		// Intervalo para actualizar los datos cada cierto tiempo (por ejemplo, cada minuto)
		const intervalId = setInterval(() => {
			fetchData();
		}, 60000); // Ajusta el intervalo según tus necesidades

		// Retornar la función de limpieza para detener el intervalo cuando el componente se desmonta
		return () => {
			clearInterval(intervalId);
		};
	}, []); // Dependencias vacías para que solo se ejecute al montar y desmontar el componente

	return (
		<Card className='h-full'>
			<CardHeader>
				<CardHeaderChild>
					<CardTitle>RMS cordenada Y</CardTitle>
				</CardHeaderChild>
			</CardHeader>
			<CardBody>
				{state.series[0].data.length === 0 ? (
					<div>Cargando datos...</div>
				) : (
					<Chart
						series={state.series}
						options={state.options}
						type={state.options.chart?.type}
						height={state.options.chart?.height}
					/>
				)}
			</CardBody>
		</Card>
	);
};

export default ChartRMSY;