import { useState, useEffect } from 'react';
import Card, { CardBody } from '../../../../components/ui/Card';
import Icon from '../../../../components/icon/Icon';
import Tooltip from '../../../../components/ui/Tooltip';
import Balance from '../../../../components/Balance';
import { TPeriod } from '../../../../constants/periods.constant';
import { accelerometerClient } from '../../../../services/accelerometer';

const Balance2Partial = ({ activeTab }: { activeTab: TPeriod }) => {
	const [statistics, setStatistics] = useState({
		min: 0,
		max: 0,
		mean: 0,
	});

	useEffect(() => {
		// Llamada a la API getAccelerometerStats para obtener las estadísticas
		const fetchData = async () => {
			try {
				const response = await accelerometerClient.getRecentAccelerometerStats('x');
				console.log('response.data', response.data);

				if (response.data) {
					// Actualizar el estado con las estadísticas recibidas de la API
					setStatistics(response.data);
				} else {
					console.warn('No data received from the API');
				}
			} catch (error) {
				console.error('Error fetching accelerometer statistics:', error);
			}
		};

		// Llamada inicial para obtener las estadísticas
		fetchData();
	}, []); // Dependencias vacías para que solo se ejecute al montar y desmontar el componente

	return (
		<Card>
			<CardBody>
				<div className='flex flex-col gap-2'>
					<div className='align-center flex flex-row'>
						<div className='flex h-16 w-16 items-center justify-center rounded-full bg-emerald-500'>
							<Icon icon='HeroTrophy' size='text-3xl' className='text-white' />
						</div>
						<div className='text-4xl font-semibold px-4'>Cordenada X</div>
					</div>
					<div className='flex flex-row'>
						<div className='flex flex-col px-4 py-1'>
							<div className='space-x-1 text-zinc-500 rtl:space-x-reverse'>
								<span className='font-semibold'>Maxima</span>
								<Tooltip text='maxima de 500 registros.' />
							</div>
							<div className='text-4xl font-semibold'>{statistics.max}</div>
						</div>
						<div className='flex flex-col px-4 py-1'>
							<div className='space-x-1 text-zinc-500 rtl:space-x-reverse'>
								<span className='font-semibold'>Minima</span>
								<Tooltip text='minima de 500 registros.' />
							</div>
							<div className='text-4xl font-semibold'>{statistics.min}</div>
						</div>
						<div className='flex flex-col px-4 py-1'>
							<div className='space-x-1 text-zinc-500 rtl:space-x-reverse'>
								<span className='font-semibold'>Media</span>
								<Tooltip text='media de 500 registros.' />
							</div>
							<div className='text-4xl font-semibold'>{statistics.avg}</div>
						</div>
						{/* <div className='flex'>
						<Balance status='negative' value='41%'>
							Balance
						</Balance>
					</div> */}
					</div>
				</div>
			</CardBody>
		</Card>
	);
};

export default Balance2Partial;
