import { useState, useEffect } from 'react';
import Card, {
	CardBody,
	CardHeader,
	CardHeaderChild,
	CardTitle,
} from '../../../../components/ui/Card';
import Chart from '../../../../components/Chart';
import { IChartOptions } from '../../../../interface/chart.interface';
import SocketClient from '../../../../services/socket-client';

const ChartFormatY = () => {
	const [state, setChartData] = useState<IChartOptions>({
		series: [
			{
				name: 'sy',
				type: 'line',
				data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			},
		],
		options: {
			chart: {
				id: 'realtime',
				height: 300,
				type: 'line',
				stacked: false,
				animations: {
					enabled: true,
					easing: 'linear',
					dynamicAnimation: {
						speed: 1000,
					},
				},
				toolbar: {
					show: true,
				},
				zoom: {
					enabled: false,
				},
			},
			dataLabels: {
				enabled: false,
			},
			stroke: {
				show: true,
				width: 4,
				colors: ['#7800FF'],
			},
			xaxis: {
				categories: [],
			},
			yaxis: {
				title: {
					text: 'Rango de aceleracion',
				},
			},
			fill: {
				opacity: 1,
			},
			tooltip: {
				shared: true,
				intersect: false,
				y: {
					formatter(y) {
						return y.toFixed(2); // Ajusta la precisión según tus necesidades
					},
				},
			},
		},
	});

	/* creamos un useEffect para que se ejecute cuando se monte el componente
	y asi mismo poder mostrar los datos en tiempo real de nuestro Socket */
	useEffect(() => {
		const socket = SocketClient.getInstance();

		// Función de callback para el evento
		const handleAccelerometerData = (data: any) => {
			//console.log('Datos del acelerómetro:', data);

			// Construir las series para cada eje
			setChartData((prevChartData) => {
				const updatedSeries = prevChartData.series.map((seriesItem) => {
					//console.log('Nombre de la serie:', seriesItem.name.toLowerCase());
					const newData = [...seriesItem.data];

					// Asegurarse de que los valores estén definidos antes de agregarlos
					if (typeof data[seriesItem.name.toLowerCase()] !== 'undefined') {
						newData.push(data[seriesItem.name.toLowerCase()].toFixed(2));
						//console.log(seriesItem.name.toLowerCase());
						//console.log('newData', newData);
					}

					// Mantener solo los últimos 10 registros
					if (newData.length > 10) {
						newData.shift();
					}

					return { ...seriesItem, data: newData };
				});

				// Actualizar las categorías del eje X
				const newCategories = [...prevChartData.options.xaxis.categories, data.id];
				if (newCategories.length > 10) {
					newCategories.shift();
				}

				return {
					...prevChartData,
					series: updatedSeries,
					options: {
						...prevChartData.options,
						xaxis: {
							...prevChartData.options.xaxis,
							categories: newCategories,
						},
					},
				};
			});
		};

		// Suscribirse al evento
		socket.onAccelerometerData(handleAccelerometerData);

		// Retornar la función de limpieza para desuscribirse cuando el componente se desmonta
		return () => {
			socket.offAccelerometerData(handleAccelerometerData);
		};
	}, []); // Dependencias vacías para que solo se ejecute al montar y desmontar el componente

	return (
		<Card className='h-full'>
			<CardHeader>
				<CardHeaderChild>
					<CardTitle>Formateado cordenada Y</CardTitle>
				</CardHeaderChild>
			</CardHeader>
			<CardBody>
				<Chart series={state.series} options={state.options} type='bar' height={400} />
			</CardBody>
		</Card>
	);
};

export default ChartFormatY;
