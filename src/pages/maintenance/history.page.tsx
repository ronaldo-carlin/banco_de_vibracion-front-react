import React, { useState, useEffect } from 'react';
import {
	createColumnHelper,
	getCoreRowModel,
	getFilteredRowModel,
	getPaginationRowModel,
	getSortedRowModel,
	SortingState,
	useReactTable,
} from '@tanstack/react-table';
import { Link } from 'react-router-dom';
import PageWrapper from '../../components/layouts/PageWrapper/PageWrapper';
import Container from '../../components/layouts/Container/Container';
import { appPages } from '../../config/pages.config';
import usersDb, { TUser } from '../../mocks/db/users.db';
import Card, { CardBody, CardHeader, CardHeaderChild, CardTitle } from '../../components/ui/Card';
import Icon from '../../components/icon/Icon';
import TableTemplate, { TableCardFooterTemplate } from '../../templates/common/TableParts.template';
import Badge from '../../components/ui/Badge';
import { DropdownItem, DropdownMenu, DropdownNavLinkItem } from '../../components/ui/Dropdown';
import Avatar from '../../components/Avatar';
import { accelerometerClient } from '../../services/accelerometer';

const columnHelper = createColumnHelper<TUser>();

const editLinkPath = `../${appPages.crmAppPages.subPages.customerPage.subPages.editPageLink.to}/`;

const columns = [
	columnHelper.accessor('id', {
		cell: (info) => <span>{info.row.original.id}</span>,
		header: 'ID',
		footer: 'ID',
		enableGlobalFilter: false,
		enableSorting: false,
	}),
	columnHelper.accessor('x', {
		cell: (info) => <span>{info.row.original.x}</span>,
		header: 'x',
		footer: 'x',
	}),
	columnHelper.accessor('y', {
		cell: (info) => <span>{info.row.original.y}</span>,
		header: 'y',
		footer: 'y',
	}),
	columnHelper.accessor('z', {
		cell: (info) => <span>{info.row.original.z}</span>,
		header: 'Z',
		footer: 'Z',
	}),
];

const CustomerListPage = () => {
	const [sorting, setSorting] = useState<SortingState>([]);
	const [globalFilter, setGlobalFilter] = useState<string>('');
	const [data, setData] = useState<TUser[]>([]);

	const table = useReactTable({
		data,
		columns,
		state: {
			sorting,
			globalFilter,
		},
		onSortingChange: setSorting,
		enableGlobalFilter: true,
		onGlobalFilterChange: setGlobalFilter,
		getCoreRowModel: getCoreRowModel(),
		getFilteredRowModel: getFilteredRowModel(),
		getSortedRowModel: getSortedRowModel(),
		getPaginationRowModel: getPaginationRowModel(),
		initialState: {
			pagination: { pageSize: 5 },
		},
	});

	useEffect(() => {
		// Llamada a la API getAccelerometer para obtener los datos
		const fetchData = async () => {
			try {
				const response = await accelerometerClient.getAccelerometer();
				console.log(response.data);
				setData(response.data); // Actualiza el estado con los datos de la API
			} catch (error) {
				console.error('Error fetching accelerometer data:', error);
			}
		};

		fetchData();
	}, []); // El segundo parámetro es un array vacío para que useEffect se ejecute solo después del montaje inicial.

	return (
		<PageWrapper name='Customer List'>
			{/* <Subheader>
				<SubheaderLeft>
					<FieldWrap
						firstSuffix={<Icon className='mx-2' icon='HeroMagnifyingGlass' />}
						lastSuffix={
							globalFilter && (
								<Icon
									icon='HeroXMark'
									color='red'
									className='mx-2 cursor-pointer'
									onClick={() => {
										setGlobalFilter('');
									}}
								/>
							)
						}>
						<Input
							id='example'
							name='example'
							placeholder='Search...'
							value={globalFilter ?? ''}
							onChange={(e) => setGlobalFilter(e.target.value)}
						/>
					</FieldWrap>
				</SubheaderLeft>
				<SubheaderRight>
					<Link to={`${editLinkPath}new`}>
						<Button variant='solid' icon='HeroPlus'>
							New Customer
						</Button>
					</Link>
				</SubheaderRight>
			</Subheader> */}
			<Container>
				<Card className='h-full'>
					<CardHeader>
						<CardHeaderChild>
							<CardTitle>Historial</CardTitle>
							<Badge
								variant='outline'
								className='border-transparent px-4'
								rounded='rounded-full'>
								{table.getFilteredRowModel().rows.length} items
							</Badge>
						</CardHeaderChild>
						<CardHeaderChild>
							<DropdownMenu placement='bottom-end'>
								<div className='grid grid-cols-12 gap-4 divide-zinc-200 dark:divide-zinc-800 md:divide-x'>
									<div className='col-span-12 gap-4 md:col-span-3'>
										<DropdownNavLinkItem to='/' icon='HeroLink'>
											Home Page
										</DropdownNavLinkItem>
										<DropdownNavLinkItem to='/ui/dropdown' icon='HeroLink'>
											Dropdown
										</DropdownNavLinkItem>
										<DropdownItem icon='HeroSquare2Stack'>Item 3</DropdownItem>
									</div>
									<div className='col-span-12 gap-4 md:col-span-3'>
										<DropdownItem icon='HeroSquare2Stack'>Item 4</DropdownItem>
										<DropdownItem icon='HeroSquare2Stack'>Item 5</DropdownItem>
										<DropdownItem icon='HeroSquare2Stack'>Item 6</DropdownItem>
									</div>
									<div className='col-span-12 gap-4 px-4 md:col-span-6'>
										Lorem ipsum dolor sit amet.
									</div>
								</div>
							</DropdownMenu>
						</CardHeaderChild>
					</CardHeader>
					<CardBody className='overflow-auto'>
						<TableTemplate className='table-fixed max-md:min-w-[70rem]' table={table} />
					</CardBody>
					<TableCardFooterTemplate table={table} />
				</Card>
			</Container>
		</PageWrapper>
	);
};

export default CustomerListPage;
