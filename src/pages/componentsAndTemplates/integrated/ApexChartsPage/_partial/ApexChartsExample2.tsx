import React, { useState, useEffect } from 'react';
import SocketClient from '../../../../../services/socket-client';
import { IChartOptions } from '../../../../../interface/chart.interface';
import Chart from '../../../../../components/Chart';

const MyComponent = () => {
	const [chartData, setChartData] = useState<IChartOptions>({
		series: [
			{
				name: 'X',
				type: 'area',
				data: [],
			},
			{
				name: 'Y',
				type: 'line',
				data: [],
			},
			{
				name: 'Z',
				type: 'line',
				data: [],
			},
		],
		options: {
			chart: {
				id: 'realtime',
				height: 300,
				type: 'line',
				stacked: false,
				animations: {
					enabled: true,
					easing: 'linear',
					dynamicAnimation: {
						speed: 1000,
					},
				},
				toolbar: {
					show: true,
				},
				zoom: {
					enabled: false,
				},
			},
			dataLabels: {
				enabled: false,
			},
			stroke: {
				/* width: [0, 2, 5], */
				curve: 'smooth',
			},
			plotOptions: {
				bar: {
					columnWidth: '50%',
				},
			},
			fill: {
				opacity: [0.85, 0.25, 1],
				gradient: {
					inverseColors: false,
					shade: 'light',
					type: 'vertical',
					opacityFrom: 0.85,
					opacityTo: 0.55,
					stops: [0, 100, 100, 100],
				},
			},
			markers: {
				size: 2,
			},
			xaxis: {
				categories: [], // Usaremos las categorías dinámicamente
				type: 'numeric',
				tickAmount: 'dataPoints',
				tickPlacement: 'on',
			},
			yaxis: {
				min: -3,
				max: 5,
			},
			tooltip: {
				shared: true,
				intersect: false,
				y: {
					formatter(y) {
						if (typeof y !== 'undefined') {
							return `${y.toFixed(0)} points`;
						}
						return y;
					},
				},
			},
		},
	});

	useEffect(() => {
		const socket = SocketClient.getInstance();

		// Escucha para recibir datos en tiempo real del acelerómetro
		socket.onAccelerometerData((data: any) => {
			console.log('Datos del acelerómetro:', data);

			// Construir las series para cada eje
			const seriesX = data.map((item: any) => {
				/* console.log('ID:', item.id); */
				// Actualizar las categorías del eje X dinámicamente con los IDs
				setChartData((prevChartData) => ({
					...prevChartData,
					options: {
						...prevChartData.options,
						xaxis: {
							...prevChartData.options.xaxis,
							categories: [...prevChartData.options.xaxis.categories, item.id, ''],
						},
					},
				}));
				/* console.log('Categorías del eje X:', chartData.options.xaxis.categories); */
				
				return item.x;
			});

			const seriesY = data.map((item: any) => item.y);
			const seriesZ = data.map((item: any) => item.z);

			// Actualizar los datos en el estado
			setChartData((prevChartData) => ({
				...prevChartData,
				series: [
					{
						...prevChartData.series[0],
						data: seriesX,
					},
					{
						...prevChartData.series[1],
						data: seriesY,
					},
					{
						...prevChartData.series[2],
						data: seriesZ,
					},
				],
			}));
		});
	}, []);

	return (
		<Chart
			series={chartData.series}
			options={chartData.options}
			type={chartData.options.chart && chartData.options.chart.type}
			height={chartData.options.chart?.height}
		/>
	);
};

export default MyComponent;
