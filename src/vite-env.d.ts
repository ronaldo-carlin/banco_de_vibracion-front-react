/// <reference types="vite/client" />

interface ImportMetaEnv {
    readonly VITE_REST_API_ENDPOINT: string
    readonly VITE_REST_API_SOCKET: string
    readonly VITE_SHOP_URL: string
    readonly VITE_APPLICATION_MODE: string
    readonly VITE_AUTH_TOKEN_KEY: string
  }
  
  interface ImportMeta {
    readonly env: ImportMetaEnv
  }