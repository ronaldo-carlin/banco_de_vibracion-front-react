import io from 'socket.io-client';

const SOCKET_ENDPOINT = import.meta.env.VITE_REST_API_SOCKET;

export class SocketClient {
	private static instance: SocketClient;
	private socket: SocketIOClient.Socket | null = null;

	private constructor() {
		this.socket = io(SOCKET_ENDPOINT);

		this.socket.on('connect', () => {
			console.log('Conectado al servidor de Socket.io');
		});

		this.socket.on('disconnect', () => {
			console.log('Desconectado del servidor de Socket.io');
		});

		this.socket.on('pong', () => {
			console.log('Pong');
		});
	}

	public static getInstance(): SocketClient {
		if (!SocketClient.instance) {
			SocketClient.instance = new SocketClient();
		}

		return SocketClient.instance;
	}

	public onAccelerometerData(callback: (data: any) => void): void {
		if (this.socket) {
			this.socket.on('socket:accelerometerData', callback);
		}
	}

	public offAccelerometerData(callback: (data: any) => void): void {
		if (this.socket) {
			this.socket.off('socket:accelerometerData', callback);
		}
	}

	public onNewRecord(callback: () => void): void {
		if (this.socket) {
			this.socket.on('nuevoRegistro', callback);
		}
	}

	public offNewRecord(callback: () => void): void {
		if (this.socket) {
			this.socket.off('nuevoRegistro', callback);
		}
	}

	public emitPing(): void {
		if (this.socket) {
			this.socket.emit('client:ping');
		}
	}

	public disconnect(): void {
		if (this.socket) {
			this.socket.disconnect();
		}
	}
}

export default SocketClient;
