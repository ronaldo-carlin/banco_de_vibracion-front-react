import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './http-client';

const accelerometer = 'accelerometer/';

export const accelerometerClient = {
	//api para traer todos los datos de los acelerometros
	getAccelerometer: () => {
		return HttpClient.get<any>(accelerometer + API_ENDPOINTS.GET_ACCELEROMETER);
	},
	getAccelerometerStats: (coordinate: string) => {
		return HttpClient.get<any>(
			`${accelerometer}${API_ENDPOINTS.GET_ACCELEROMETERSTATS}?coordinate=${coordinate}`,
		);
	},
	getRecentAccelerometerStats: (coordinate: string) => {
		return HttpClient.get<any>(
			`${accelerometer}${API_ENDPOINTS.GET_RECENTACCELEROMETERSTATS}?coordinate=${coordinate}`,
		);
	},
};