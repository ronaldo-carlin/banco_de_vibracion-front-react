import {
	AuthResponse,
	LoginInput,
	RegisterInput,
	User,
	ChangePasswordInput,
	ForgetPasswordInput,
	VerifyForgetPasswordTokenInput,
	ResetPasswordInput,
	MakeAdminInput,
	BlockUserInput,
	WalletPointsInput,
	UpdateUser,
	QueryOptionsType,
	UserPaginator,
	UserQueryOptions,
} from '../types';
import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './http-client';

const AUTH = 'auth/';

export const userClient = {
	login: (variables: LoginInput) => {
		return HttpClient.post<AuthResponse>(AUTH+API_ENDPOINTS.LOGIN, variables);
	},
	logout: () => {
		return HttpClient.post<any>(API_ENDPOINTS.LOGOUT, {});
	},
	register: (variables: RegisterInput) => {
		return HttpClient.post<AuthResponse>(API_ENDPOINTS.REGISTER, variables);
	},
	update: ({ id, input }: { id: string; input: UpdateUser }) => {
		return HttpClient.put<User>(`${API_ENDPOINTS.USERS}/${id}`, input);
	},
	changePassword: (variables: ChangePasswordInput) => {
		return HttpClient.post<any>(API_ENDPOINTS.CHANGE_PASSWORD, variables);
	},
	forgetPassword: (variables: ForgetPasswordInput) => {
		return HttpClient.post<any>(API_ENDPOINTS.FORGET_PASSWORD, variables);
	},
	verifyForgetPasswordToken: (variables: VerifyForgetPasswordTokenInput) => {
		return HttpClient.post<any>(API_ENDPOINTS.VERIFY_FORGET_PASSWORD_TOKEN, variables);
	},
	resetPassword: (variables: ResetPasswordInput) => {
		return HttpClient.post<any>(API_ENDPOINTS.RESET_PASSWORD, variables);
	},
};
