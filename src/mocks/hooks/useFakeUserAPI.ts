import { useEffect, useState } from 'react';
import usersDb, { TUser } from '../db/users.db';
import { userClient } from '../../services/users';

const useFakeUserAPI = (username: string) => {
	const [isLoading, setIsLoading] = useState<boolean>(true);
	const [response, setResponse] = useState<TUser | undefined>();

	const getCheckUser = async (userNameOrMail: string, password: string) => {
		try {
			const authResponse = await userClient.login({
				user: userNameOrMail,
				password: password,
			});

			// You may want to handle the response accordingly
			return authResponse;
		} catch (error) {
			// Handle errors
			//console.error('Error con endpoint:', error);

			if (error.response && error.response.status === 404) {
				// Unauthorized - incorrect username or password
				throw new Error(error.response.data.error);
			} else {
				// Other errors
				throw error;
			}
		}
	};

	useEffect(() => {
		const fetchData = async () => {
			try {
				//const meResponse = await userClient.me();
				//setResponse(meResponse);
				setIsLoading(false);
			} catch (error) {
				// Handle errors
				console.error('Error fetching user data:', error);
				setIsLoading(false);
			}
		};

		fetchData();

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [username]);

	return { response, isLoading, getCheckUser };
};

export default useFakeUserAPI;
