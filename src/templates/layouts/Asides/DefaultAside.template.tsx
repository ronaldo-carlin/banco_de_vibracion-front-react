import { useNavigate } from 'react-router-dom';
import Aside, { AsideBody, AsideFooter, AsideHead } from '../../../components/layouts/Aside/Aside';
import LogoAndAsideTogglePart from './_parts/LogoAndAsideToggle.part';
import DarkModeSwitcherPart from './_parts/DarkModeSwitcher.part';
import { appPages } from '../../../config/pages.config';
import Nav, {
	NavItem,
	NavSeparator,
	NavTitle,
} from '../../../components/layouts/Navigation/Nav';
import UserTemplate from '../User/User.template';

const DefaultAsideTemplate = () => {
	const navigate = useNavigate();

	return (
		<Aside>
			<AsideHead>
				<LogoAndAsideTogglePart />
			</AsideHead>
			<AsideBody>
				<Nav>
					<NavItem {...appPages.salesAppPages.subPages.salesDashboardPage} />

					<NavTitle>Mantenimiento</NavTitle>

					<NavItem
						text={appPages.historyAppPages.text}
						to={appPages.historyAppPages.to}
						icon={appPages.historyAppPages.icon}></NavItem>
					<NavSeparator />

					<NavTitle>Administrador</NavTitle>

					<NavItem {...appPages.crmAppPages.subPages.customerPage.subPages.listPage} />

				</Nav>
			</AsideBody>
			<AsideFooter>
				<UserTemplate />
				<DarkModeSwitcherPart />
			</AsideFooter>
		</Aside>
	);
};

export default DefaultAsideTemplate;
